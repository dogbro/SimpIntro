// 本页需要存储元素
// 1.userEduArr
// 2.userWorkArr
// example：
// 1.{"eduName":this.data.eduName,"eduType":this.data.eduType,"eduSchName":this.data.eduSchName,"eduStartTime":this.data.eduStartTime,"eduEndTime":this.data.eduEndTime}
// 2.{"srcArr":this.data.srcArr,"companyNameArr":this.data.companyNameArr,"workTypeArr":this.data.workTypeArr,"workDetailArr":this.data.workDetailArr,"workBeginDateArr":this.data.workBeginDateArr,"workEndDateArr":this.data.workEndDateArr}

var headerImg = '../../pages/resources/1_1PictureHolder.png'
Page({
  data: {
    index: 0,
    workIndex: 0,
    remindText: "",
    modalHidden: true,
    modalHidden2: true,
    modalHidden3: true,
    textareaDisplay: "none",
    model_title_work: "添加",
    beginDate: "2016-09-01",
    endDate: "2016-09-01",
    eduExprenceArr: ["小学", "初中", "高中", "本科", "硕士"],
    addTheme: "小学",
    selectIndex: 0,
    model_title_detail: "",
    addName: "",
    eduName: [],
    eduType: [],
    eduSchName: [],
    eduStartTime: [],
    eduEndTime: [],

    //工作经历字段
    src: headerImg,
    companyName: "",
    workType: "",
    workDetail: "",
    workBeginDate: "2016-09-01",
    workEndDate: "2016-09-01",
    srcArr: [],
    companyNameArr: [],
    workTypeArr: [],
    workDetailArr: [],
    workBeginDateArr: [],
    workEndDateArr: []
  },
  getLocalHeader: function () {
    var that = this
    wx.chooseImage({
      count: 1, // 默认9
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        var tempFilePaths = res.tempFilePaths
        that.setData({
          src: tempFilePaths
        })
        headerImg = tempFilePaths
        console.log(headerImg)
      }
    })
  },
  modalCancel: function (e) {
    var flag = e.target.dataset.flag
    switch (flag) {
      case "0":
        this.setData({
          modalHidden: true
        })
        break;
      case "1":
        this.setData({
          modalHidden2: true,
          textareaDisplay: "none"
        })
        break;
      case "2":
        this.setData({
          modalHidden3: true
        })
        break;
    }
  },
  modalChange: function (e) {
    var flag = e.target.dataset.flag
    switch (flag) {
      case "0":
        if (this.data.model_title_detail == "添加") {
          var tempEduName = this.data.eduName
          var eduType = this.data.eduType
          var eduSchName = this.data.eduSchName
          var eduStartTime = this.data.eduStartTime
          var eduEndTime = this.data.eduEndTime
          if (this.data.addName != "") {
            tempEduName.push(this.data.addTheme + ' ---- ' + this.data.addName)
            eduType.push(this.data.addTheme)
            eduSchName.push(this.data.addName)
            eduStartTime.push(this.data.beginDate)
            eduEndTime.push(this.data.endDate)
            this.setData({
              eduType: eduType,
              eduName: tempEduName,
              eduSchName: eduSchName,
              eduStartTime: eduStartTime,
              eduEndTime: eduEndTime,
              modalHidden: true
            })
          } else {
            this.setData({
              modalHidden: true,
              modalHidden3: false,
              remindText: "空的！哥，得填写信息啊"
            })
          }
        } else {
          var index = this.data.selectIndex
          console.log(index)
          var tempEduName = this.data.eduName
          var eduType = this.data.eduType
          var eduSchName = this.data.eduSchName
          var eduStartTime = this.data.eduStartTime
          var eduEndTime = this.data.eduEndTime

          tempEduName[index] = this.data.addTheme + ' ---- ' + this.data.addName
          eduType[index] = this.data.addTheme
          eduSchName[index] = this.data.addName
          eduStartTime[index] = this.data.beginDate
          eduEndTime[index] = this.data.endDate

          this.setData({
            eduType: eduType,
            eduName: tempEduName,
            eduSchName: eduSchName,
            eduStartTime: eduStartTime,
            eduEndTime: eduEndTime,
            modalHidden: true
          })
        }
        break;
      case "1":
        if (this.data.model_title_work == "添加") {
          var srcArr = this.data.srcArr
          var companyNameArr = this.data.companyNameArr
          var workTypeArr = this.data.workTypeArr
          var workDetailArr = this.data.workDetailArr
          var workBeginDateArr = this.data.workBeginDateArr
          var workEndDateArr = this.data.workEndDateArr
          if (this.data.companyName == "" || this.data.workType == "" || this.data.workDetail == "") {
            this.setData({
              modalHidden2: true,
              modalHidden3: false,
              textareaDisplay: "none",
              remindText: "你好像忘了填写什么信息啊"
            })
          } else {
            srcArr.push(this.data.src)
            companyNameArr.push(this.data.companyName)
            workTypeArr.push(this.data.workType)
            workDetailArr.push(this.data.workDetail)
            workBeginDateArr.push(this.data.workBeginDate)
            workEndDateArr.push(this.data.workEndDate)
            this.setData({
              srcArr: srcArr,
              companyNameArr: companyNameArr,
              workTypeArr: workTypeArr,
              workDetailArr: workDetailArr,
              workBeginDateArr: workBeginDateArr,
              workEndDateArr: workEndDateArr,
              textareaDisplay: "none"
            })
          }
          this.setData({
            modalHidden2: true
          })
        } else {
          //修改的情况
          var flag = this.data.workIndex
          var srcArr = this.data.srcArr
          var companyNameArr = this.data.companyNameArr
          var workTypeArr = this.data.workTypeArr
          var workDetailArr = this.data.workDetailArr
          var workBeginDateArr = this.data.workBeginDateArr
          var workEndDateArr = this.data.workEndDateArr

          srcArr[flag] = this.data.src
          companyNameArr[flag] = this.data.companyName
          workTypeArr[flag] = this.data.workType
          workDetailArr[flag] = this.data.workDetail
          workBeginDateArr[flag] = this.data.workBeginDate
          workEndDateArr[flag] = this.data.workEndDate
          this.setData({
            srcArr: srcArr,
            companyNameArr: companyNameArr,
            workTypeArr: workTypeArr,
            workDetailArr: workDetailArr,
            workBeginDateArr: workBeginDateArr,
            workEndDateArr: workEndDateArr,
            textareaDisplay: "none"
          })
        }
        break;
      case "2":
        this.setData({
          modalHidden3: true
        })
        break;
    }
  },
  add: function (e) {
    var flag = e.target.dataset.flag
    switch (flag) {
      case "0":
        this.setData({
          addName: "",
          model_title_detail: "添加",
          modalHidden: false
        })
        break;
      case "1":
        this.setData({
          src: '../../pages/resources/1_1PictureHolder.png',
          companyName: "",
          workType: "",
          workDetail: "",
          workBeginDate: "2016-09-01",
          workEndDate: "2016-09-01",
          textareaDisplay: "block",
          model_title_work: "添加",
          modalHidden2: false
        })
        console.log(this.data.src)
        break;
    }
  },
  bindPickerChange: function (e) {
    this.setData({
      index: e.detail.value,
      addTheme: this.data.eduExprenceArr[e.detail.value]
    })
  },
  editWorkExp: function (e) {

  },
  bindDateChange: function (e) {
    var flag = e.target.dataset.flag
    switch (flag) {
      case "0":
        this.setData({
          beginDate: e.detail.value
        })
        break;
      case "1":
        this.setData({
          endDate: e.detail.value
        })
        break;
      case "2":
        this.setData({
          workBeginDate: e.detail.value
        })
        break;
      case "3":
        this.setData({
          workEndDate: e.detail.value
        })
        break;
    }
  },
  companyName: function (e) {
    this.setData({
      companyName: e.detail.value
    })
  },
  workType: function (e) {
    this.setData({
      workType: e.detail.value
    })
  },
  workDetail: function (e) {
    this.setData({
      workDetail: e.detail.value
    })
    console.log(this.data.workDetail)
  },
  InputContact: function (e) {
    this.setData({
      addName: e.detail.value
    })
  },
  //事件处理函数
  clickToNextView: function () {
    if (this.data.eduType.length == 0 || this.data.workTypeArr.length == 0) {
      console.log("没有填写偶写信息")
      if (this.data.eduType.length == 0) {
        console.log("No edu")
        this.setData({
          modalHidden3: false,
          remindText: "最起码填写一个教育经历吧？"
        })
      } else {
        console.log("No work")
        this.setData({
          modalHidden3: false,
          remindText: "最起码填写一个工作经历吧？"
        })
      }
    } else {
      wx.setStorageSync('userEduArr', { "eduName": this.data.eduName, "eduType": this.data.eduType, "eduSchName": this.data.eduSchName, "eduStartTime": this.data.eduStartTime, "eduEndTime": this.data.eduEndTime })
      wx.setStorageSync('userWorkArr', { "srcArr": this.data.srcArr, "companyNameArr": this.data.companyNameArr, "workTypeArr": this.data.workTypeArr, "workDetailArr": this.data.workDetailArr, "workBeginDateArr": this.data.workBeginDateArr, "workEndDateArr": this.data.workEndDateArr })
      // wx.navigateTo({
      //   url: '../EWexperience/EWexperience'
      // })
    }
  },
  editDetail: function (e) {
    var flag = e.target.dataset.flag
    switch (flag) {
      case "0":
        var index = e.target.dataset.index
        var indexNum = this.data.eduName.indexOf(index)
        this.setData({
          modalHidden: false,
          workIndex: index,
          model_title_detail: "修改",
          beginDate: this.data.eduStartTime[indexNum],
          endDate: this.data.eduEndTime[indexNum],
          addTheme: this.data.eduType[indexNum],
          addName: this.data.eduSchName[indexNum],
          selectIndex: indexNum
        })
        break;
      case "1":
        var indexNum = e.target.dataset.index
        this.setData({
          modalHidden2: false,
          textareaDisplay: "block",
          model_title_work: "修改",
          src: this.data.srcArr[indexNum],
          companyName: this.data.companyNameArr[indexNum],
          workType: this.data.workTypeArr[indexNum],
          workDetail: this.data.workDetailArr[indexNum],
          workBeginDate: this.data.workBeginDateArr[indexNum],
          workEndDate: this.data.workEndDateArr[indexNum]
        })
    }

  },
  remove: function (e) {
    var flag = e.target.dataset.flag
    switch (flag) {
      case "0":
        var index = e.target.dataset.index
        var indexNum = this.data.eduName.indexOf(index)
        this.data.eduName.splice(indexNum, 1)
        this.data.eduType.splice(indexNum, 1)
        this.data.eduSchName.splice(indexNum, 1)
        this.data.eduStartTime.splice(indexNum, 1)
        this.data.eduEndTime.splice(indexNum, 1)

        var tempEduName = this.data.eduName
        var eduType = this.data.eduType
        var eduSchName = this.data.eduSchName
        var eduStartTime = this.data.eduStartTime
        var eduEndTime = this.data.eduEndTime

        this.setData({
          eduType: eduType,
          eduName: tempEduName,
          eduSchName: eduSchName,
          eduStartTime: eduStartTime,
          eduEndTime: eduEndTime
        })
        break;
      case "1":
        var indexNum = e.target.dataset.index
        console.log(indexNum)
        this.data.srcArr.splice(indexNum, 1)
        this.data.companyNameArr.splice(indexNum, 1)
        this.data.workTypeArr.splice(indexNum, 1)
        this.data.workDetailArr.splice(indexNum, 1)
        this.data.workBeginDateArr.splice(indexNum, 1)
        this.data.workEndDateArr.splice(indexNum, 1)
        var srcArr = this.data.srcArr
        var companyNameArr = this.data.companyNameArr
        var workTypeArr = this.data.workTypeArr
        var workDetailArr = this.data.workDetailArr
        var workBeginDateArr = this.data.workBeginDateArr
        var workEndDateArr = this.data.workEndDateArr
        this.setData({
          srcArr: srcArr,
          companyNameArr: companyNameArr,
          workTypeArr: workTypeArr,
          workDetailArr: workDetailArr,
          workBeginDateArr: workBeginDateArr,
          workEndDateArr: workEndDateArr
        })
        break;
    }
  }
})