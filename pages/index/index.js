//index.js
//获取应用实例
//本页存储元素
//1.userSex
var app = getApp()
Page({
  data: {
    motto: '王狗简历',
    sexPic: "../../pages/resources/man.png",
    userSex:"0",
    userInfo: {
    }
  },
  //事件处理函数
  bindViewTap: function() {
    wx.setStorageSync('userSex',this.data.userSex)
    wx.navigateTo({
      url: '../name$Header/name$Header'
    })
  },
  switchSex:function(){
    if (this.data.sexPic == "../../pages/resources/man.png"){
      this.setData({
        sexPic: "../../pages/resources/women.png"
      })
    }else{
      this.setData({
        sexPic: "../../pages/resources/man.png"
      })
    }
  },
  switchImg:function(e){
    var flag = e.target.dataset.flag
    if (flag == "0"){
      this.setData({
        sexPic: "../../pages/resources/man.png"
      })
    }else{
      this.setData({
        sexPic: "../../pages/resources/women.png"
      })
    }
  },
  onLoad: function () {
    console.log('first_onLoad')
    var that = this
  	//调用应用实例的方法获取全局数据
    app.getUserInfo(function(userInfo){
      //更新数据
      that.setData({
        userInfo:userInfo
      })
      if (userInfo.gender == 1){
      that.setData({
        userSex:"1",
        sexPic: "../../pages/resources/man.png"
      })
    }else{
        that.setData({
          userSex:"0",
        sexPic: "../../pages/resources/women.png"
      })
    }
      that.update()
    })
  }
})
