// pages/name$Header/name$Header.js
import { Promise } from '../../utils/util';

/**
 *  查询接口
 */
// const API = 'http://japi.zto.cn/zto/api_utf8/baseArea?msg_type=GET_AREA&data=';
/**
 *本页存储key
 *1.userName
 *2.userAddress
 *3.userHeader
 */


var app = getApp()
var headerImg = '../../pages/resources/1_1PictureHolder.png'
Page({
  data: {
    src: headerImg,
    modalHidden: true,
    modalHidden2: true,
    inputValue: "",
    AddressValue: "",
    selectAddress: null
  },
  addDot: function (arr) {
    if (arr instanceof Array) {
      arr.map(val => {
        if (val.fullName.length > 4) {
          val.fullNameDot = val.fullName.slice(0, 4) + '...';
          return val;
        } else {
          val.fullNameDot = val.fullName;
          return val;
        }
      })
    }
  },
  onLoad: function (options) {
    var that = this
    // 页面初始化 options为页面跳转所带来的参数
    console.log('onLoad')
    //调用应用实例的方法获取全局数据
    // app.getUserInfo(function (userInfo) {
    //   //更新数据
    //   that.setData({
    //     userInfo: userInfo
    //   })
    //   that.update()
    // })
    // that.setData({
    //   isShow: false, // 显示区域选择框
    //   inputViewDisplay:"block",
    //   showDistrict: true // 默认为省市区三级区域选择
    // });
    // Promise(wx.request, {
    //   url: API + '0',
    //   method: 'GET'
    // }).then((province) => {
    //   const firstProvince = province.data.result[0];
    //   that.addDot(province.data.result);
    //   /**
    //    * 默认选择获取的省份第一个省份数据
    //    */
    //   that.setData({
    //     proviceData: province.data.result,
    //     'selectedProvince.index': 0,
    //     'selectedProvince.code': firstProvince.code,
    //     'selectedProvince.fullName': firstProvince.fullName,
    //   });
    //   return (
    //     Promise(wx.request, {
    //       url: API + firstProvince.code,
    //       method: 'GET'
    //     })
    //   );
    // }).then((city) => {
    //   const firstCity = city.data.result[0];
    //   that.addDot(city.data.result);
    //   that.setData({
    //     cityData: city.data.result,
    //     'selectedCity.index': 0,
    //     'selectedCity.code': firstCity.code,
    //     'selectedCity.fullName': firstCity.fullName,
    //   });
    //   /**
    //    * 省市二级则不请求区域
    //    */
    //   if (that.data.showDistrict) {
    //     return (
    //       Promise(wx.request, {
    //         url: API + firstCity.code,
    //         method: 'GET'
    //       })
    //     );
    //   } else {
    //     that.setData({
    //       value: [0, 0]
    //     });
    //     return;
    //   }
    // }).then((district) => {
    //   const firstDistrict = district.data.result[0];
    //   that.addDot(district.data.result);
    //   that.setData({
    //     value: [0, 0, 0],
    //     districtData: district.data.result,
    //     'selectedDistrict.index': 0,
    //     'selectedDistrict.code': firstDistrict.code,
    //     'selectedDistrict.fullName': firstDistrict.fullName,
    //     address: null
    //   });
    // }).catch((e) => {
    //   console.log(e);
    // })
  },
  getLocalHeader: function () {
    var that = this
    wx.chooseImage({
      count: 1, // 默认9
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        var tempFilePaths = res.tempFilePaths
        that.setData({
          src: tempFilePaths
        })
        headerImg = tempFilePaths
        console.log(headerImg)
      }
    })
  },
  //事件处理函数
  bindViewTap: function () {
    if (this.data.src != headerImg && this.data.inputValue != "") {
      //存储信息
      wx.setStorageSync('userName', this.data.inputValue)
      wx.setStorageSync('userAddress',this.data.AddressValue)
      wx.setStorageSync('userHeader',this.data.src)
      wx.navigateTo({
        url: '../birth&detail/birth&detail'
      })
    } else {
      if (this.data.src != headerImg) {
        console.log("没有修改名字！")
        this.setData({
          modalHidden: false
        })
      } else {
        console.log("没有修改头像头像！")
        this.setData({
          modalHidden2: false
        })
      }
    }

  },
  clickChooseAddress: function (e) {
    console.log("点击修改地址")
  },
  //处理弹出框隐藏
  modalChange2: function (e) {
    this.setData({
      modalHidden2: true
    })
  },
  //处理弹出框隐藏
  modalChange: function (e) {
    this.setData({
      modalHidden: true
    })
  },
  //记录屏幕的input输入
  bindKeyInput: function (e) {
    var flag = e.target.dataset.flag
    console.log(flag)
    switch (flag) {
      case "0":
        this.setData({
          inputValue: e.detail.value
        })
        console.log(this.data.inputValue)
        break;
      case "1":
        this.setData({
          AddressValue: e.detail.value
        })
        console.log(this.data.AddressValue)
    }

  },
  /**
   * 页面选址触发事件
   */
  choosearea: function () {
    this.setData({
      isShow: true,
      inputViewDisplay: "none"
    })
  },
  /**
   * 滑动事件
   */
  bindChange: function (e) {
    const current_value = e.detail.value, _data = this.data;

    console.log('ddddddddd');

    if (current_value.length > 2) {
      if (this.data.value[0] !== current_value[0] && this.data.value[1] === current_value[1] && this.data.value[2] === current_value[2]) {
        // 滑动省份
        Promise(wx.request, {
          url: API + _data.proviceData[current_value[0]].code,
          method: 'GET'
        }).then((city) => {
          this.addDot(city.data.result);
          this.setData({
            cityData: city.data.result
          })
          return (
            Promise(wx.request, {
              url: API + city.data.result[0].code,
              method: 'GET'
            })
          );
        }).then((district) => {
          if (district.data.result.length > 0) {
            this.addDot(district.data.result);
            this.setData({
              districtData: district.data.result,
              'value[0]': current_value[0],
              'value[1]': 0,
              'value[2]': 0
            })

            this.setData({
              address: this.data.proviceData[current_value[0]].fullName + ' - ' + this.data.cityData[0].fullName + ' - ' + district.data.result[0].fullName
            })
          }


        }).catch((e) => {
          console.log(e);
        })
      } else if (this.data.value[0] === current_value[0] && this.data.value[1] !== current_value[1] && this.data.value[2] === current_value[2]) {
        // 滑动城市
        Promise(wx.request, {
          url: API + _data.cityData[current_value[1]].code,
          method: 'GET'
        }).then((district) => {
          if (district.data.result.length > 0) {
            this.addDot(district.data.result);
            this.setData({
              districtData: district.data.result,
              'value[0]': current_value[0],
              'value[1]': current_value[1],
              'value[2]': 0,
              address: this.data.proviceData[current_value[0]].fullName + ' - ' + this.data.cityData[current_value[1]].fullName + ' - ' + district.data.result[0].fullName
            })
          }
        }).catch((e) => {
          console.log(e);
        })

      } else if (this.data.value[0] === current_value[0] && this.data.value[1] === current_value[1] && this.data.value[2] !== current_value[2]) {
        // 滑动地区
        this.setData({
          value: current_value,
          address: this.data.proviceData[current_value[0]].fullName + ' - ' + this.data.cityData[current_value[1]].fullName + ' - ' + this.data.districtData[current_value[2]].fullName
        })
      }
    }
  },
  clickCancelBtn: function (e) {
    this.setData({
      isShow: false,
      inputViewDisplay: "block"
    })
  },
  clickconfirmBtn: function (e) {
    this.setData({
      isShow: false,
      inputViewDisplay: "block",
      selectAddress: this.data.address
    })
  }
})