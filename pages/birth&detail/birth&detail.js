// 本页存储元素
// 1.userBirth
// 2.userContactMap

Page({
  data: {
    index: 0,
    date: "2016-09-01",
    time: "12:01",
    modalHidden: true,
    modalHidden2: true,
    extraAddPic: [],
    array: ["邮箱", "QQ", "Tel", "微信"],
    addTheme: "邮箱",
    addText: "",
    remindText: "",
    contact_inputType: "text",
    userInfo_QQ: "",
    userInfo_Mail: "",
    userInfo_WeChat: "",
    userInfo_Tel: "",

    popUp_Pic: "",
    popUp_Title: "",
    popUp_Contant: "",
  },
  bindPickerChange: function (e) {
    switch (e.detail.value) {
      case "QQ" || "Tel":
        this.setData({
          contact_inputType: "number"
        })
        break;
      default:
        this.setData({
          contact_inputType: "text"
        })
        break;
    }
    this.setData({
      index: e.detail.value,
      addTheme: this.data.array[e.detail.value]
    })
  },
  bindDateChange: function (e) {
    this.setData({
      date: e.detail.value
    })
  },
  previewImage: function (e) {
    var current = e.target.dataset.src
  },
  modalChange: function (e) {
    if (this.data.addText == "") {
      this.setData({
        remindText: "你什么都没填，点确认是几个意思？",
        modalHidden2: false
      })
    } else {
      switch (this.data.addTheme) {
        case "QQ":
          if (this.data.addPicArray == null) {
            this.setData({
              addPicArray: ["../../pages/resources/Tweetbot.png"],
              userInfo_QQ: this.data.addText
            })
          } else {
            if (this.data.addPicArray.indexOf("../../pages/resources/Tweetbot.png") != -1) {
              this.setData({
                remindText: "小伙子，你QQ很多啊",
                modalHidden2: false
              })
            } else {
              this.data.addPicArray.push("../../pages/resources/Tweetbot.png")
              this.data.extraAddPic = this.data.addPicArray
              console.log(this.data.extraAddPic)
              this.setData({
                addPicArray: this.data.extraAddPic,
                userInfo_QQ: this.data.addText
              })
            }
          };
          break;
        case "Tel":
          if (this.data.addPicArray == null) {
            this.setData({
              addPicArray: ["../../pages/resources/WhatsApp.png"],
              userInfo_Tel: this.data.addText
            })
          } else {
            if (this.data.addPicArray.indexOf("../../pages/resources/WhatsApp.png") != -1) {
              this.setData({
                remindText: "小伙子，你手机很多啊",
                modalHidden2: false
              })
            } else {
              this.data.addPicArray.push("../../pages/resources/WhatsApp.png")
              this.data.extraAddPic = this.data.addPicArray
              console.log(this.data.extraAddPic)
              this.setData({
                addPicArray: this.data.extraAddPic,
                userInfo_Tel: this.data.addText
              })
            }
          };
          break;
        case "微信":
          if (this.data.addPicArray == null) {
            this.setData({
              addPicArray: ["../../pages/resources/WeChat.png"],
              userInfo_WeChat: this.data.addText
            })
          } else {
            if (this.data.addPicArray.indexOf("../../pages/resources/WeChat.png") != -1) {
              this.setData({
                remindText: "小伙子，你微信很多啊",
                modalHidden2: false
              })
            } else {
              this.data.addPicArray.push("../../pages/resources/WeChat.png")
              this.data.extraAddPic = this.data.addPicArray
              console.log(this.data.extraAddPic)
              this.setData({
                addPicArray: this.data.extraAddPic,
                userInfo_WeChat: this.data.addText
              })
            }
          };
          break;
        case "邮箱":
          if (this.data.addPicArray == null) {
            this.setData({
              addPicArray: ["../../pages/resources/Gmail.png"],
              userInfo_Mail: this.data.addText
            })
          } else {
            if (this.data.addPicArray.indexOf("../../pages/resources/Gmail.png") != -1) {
              this.setData({
                remindText: "小伙子，你邮箱很多啊",
                modalHidden2: false
              })
            } else {
              this.data.addPicArray.push("../../pages/resources/Gmail.png")
              this.data.extraAddPic = this.data.addPicArray
              console.log(this.data.extraAddPic)
              this.setData({
                addPicArray: this.data.extraAddPic,
                userInfo_Mail: this.data.addText
              })
            }
          };
          break;
        default:
          break;
      }
    }
    this.setData({
      modalHidden: true,
    })
  },
  modalCancel: function (e) {
    this.setData({
      modalHidden: true
    })
  },
  modalChange2: function (e) {
    this.setData({
      modalHidden2: true,
    })
  },
  InputContact: function (e) {
    this.setData({
      addText: e.detail.value
    })
  },
  add: function (e) {
    this.setData({
      addText: "",
      modalHidden: false
    })
  },
  remove: function (e) {
    if (this.data.addPicArray != null) {
      if (this.data.addPicArray.length > 0) {
        this.data.addPicArray.pop()
        this.setData({
          addPicArray: this.data.addPicArray
        })
      }
    } else {
      this.setData({
        remindText: "都没了你还减？",
        modalHidden2: false
      })
    }
  },
  //事件处理函数
  clickToNextView: function() {
    if (this.data.date == "2016-09-01"||(this.data.userInfo_Mail == "" && this.data.userInfo_WeChat == "" && this.data.userInfo_QQ == "" && this.data.userInfo_Tel == "")){
      if (this.data.date == "2016-09-01"){
        this.setData({
          remindText: "你好像没有填写出生日期？",
          modalHidden2: false
        })
      }else{
        this.setData({
          remindText: "一个联系方式都不填？你确定？",
          modalHidden2: false
        })
      }
    }else{
      wx.setStorageSync('userBirth',this.data.date)
      wx.setStorageSync('userContant',{"QQ":this.data.userInfo_QQ,"WeChat":this.data.userInfo_WeChat,"Tel":this.data.userInfo_Tel,"Mail":this.data.userInfo_Mail})
      wx.navigateTo({
        url: '../EWexperience/EWexperience'
      })
    }
    
  },
  previewImage: function (e) {
    var current = e.target.dataset.src
    switch (current) {
      case "../../pages/resources/Tweetbot.png":
        this.clickPic_popUp("QQ", this.data.userInfo_QQ);
        break;
      case "../../pages/resources/Gmail.png":
        this.clickPic_popUp("Mail", this.data.userInfo_Mail);
        break;
      case "../../pages/resources/WeChat.png":
        this.clickPic_popUp("WeChat", this.data.userInfo_WeChat);
        break;
      case "../../pages/resources/WhatsApp.png":
        this.clickPic_popUp("手机号码", this.data.userInfo_Tel);
        break;
      default:
        break;
    }
  },
  clickPic_popUp: function (title, content) {
    var that = this
    wx.showModal({
      title: title,
      content: content,
      cancelText: '删除',
      success: function (res) {
        if (res.confirm) {
          console.log('用户点击确定')
        } else {
          console.log('用户点击删除')
          switch (title) {
            case "QQ":
              //删除数组中对应元素
              var indexNum = that.data.addPicArray.indexOf("../../pages/resources/Tweetbot.png")
              that.data.addPicArray.splice(indexNum, 1)
              var newArray = that.data.addPicArray
              that.setData({
                addPicArray: newArray,
                userInfo_QQ: ""
              })
              break;
            case "Mail":
              //删除数组中对应元素
              var indexNum = that.data.addPicArray.indexOf("../../pages/resources/Gmail.png")
              that.data.addPicArray.splice(indexNum, 1)
              var newArray = that.data.addPicArray
              that.setData({
                addPicArray: newArray,
                userInfo_Mail: ""
              })
              break;
            case "WeChat":
              //删除数组中对应元素
              var indexNum = that.data.addPicArray.indexOf("../../pages/resources/WeChat.png")
              that.data.addPicArray.splice(indexNum, 1)
              var newArray = that.data.addPicArray
              that.setData({
                addPicArray: newArray,
                userInfo_WeChat: ""
              })
              break;
            case "手机号码":
              //删除数组中对应元素
              var indexNum = that.data.addPicArray.indexOf("../../pages/resources/WhatsApp.png")
              that.data.addPicArray.splice(indexNum, 1)
              var newArray = that.data.addPicArray
              that.setData({
                addPicArray: newArray,
                userInfo_Tel: ""
              })
              break;
            default:
              break;
          }
        }
      }
    })
  },
})
